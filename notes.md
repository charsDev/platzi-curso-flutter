# 0. Setup de repositorio
* Setupeando el repositorio y el archivo notes.md

# 1. Bienvenida e introduccion
## 1.1 ¿Que es Flutter?
* Es un SDK de desarrollo creado por Google pensado en desarollar interfaces nativas de Android y iOS y que tengan el mejor rendimiento.
* Utiliza el lenguaje de programacion __Dart__ y fue creado para programadores de interfaces de moviles.
* Flutter esta en la categoria closs platform ya que genera apps casi nativas. Esta mas adelantada a React Native o Xamarin ya que ellos usan un bridge que lleva a la app por un Native Control y Render; Flutter por el contrario no utiliza ningun bridge. NO COMPILA EN BRIDGE.
* Esto da como resultado apps mas veloces, optimizadas, interfaces se sienten mucho mejor y renderean mas rapida.
* Skia 2D engine(motor) de renderizado de Flutter.

## 1.2 Dart y Flutter
* Diferencias entre Dart vs Jva Vs React
* Dart funciona para web mediante Angular Dart y tambien funciona para server

## 1.3 Sintaxis de Dart 
* [Resumen de la sintaxis de Dart](https://ibb.co/DbKKtjX)

## 1.4 Flutter para desarrolladores Android, iOS y Xamarin.forms
* __En Android:__ 
  * Todo es un View
  * Usamos XML
* __En iOS:__
  * Tod es un UIView
  * Utiliza Storyboard entorno grafico
* __En Xamarin:__
  * Todo es un Element
  * Utiliza XAML parecido a XML
* __En flutter:__
  * Todo es un Widget
  * Usaremos Widget Tree(Widgetas anidados) lo que seria XML en Android

## 1.5 Flutter para desarrolladores React Native
* Si desarrollas aplicaciones con React Native ya habrás notado que hacerlo con Flutter es muy similar pues utiliza el mismo estilo reactivo.

* La principal diferencia es que mientras que React Native transpila (traduce) el código a Widgets Nativos para cada plataforma, Flutter compila todo directamente a Nativo controlando cada pixel de la pantalla para evitar problemas de rendimiento causados por el bridge de JavaScript.

## 1.6 ¿Como luce una app construída en Flutter?
* *“Mas que posicionar a Android, mas que posicionar a Flutter, es esto es por tener mejores aplicaciones”*
* [Startflutter](https://startflutter.com/)

# 2. Creando mi entorno de desarrollo
* Aqui hablaron e los requerimientos para usar flutter, instalacion Android Studio.
* En la composicion de un proyecto de flutter, creamos el proyecto y se mencionaron las diferentes carpetas que tiene el proyecto.

# 3. Interfaces en Flutter
## 3.1 Programacion Declarativa en Flutter
* Programacion Imperativa es la que se esta acostumbrado a usar, POO, se utilizan metodos.
* Programacion Declarativa es la que usa Dart y Flutter en la cual asignamos un valor a la propiedad que deseemos.
* [Video del tema](https://www.youtube.com/watch?v=poDSwUIQVzA)

## 3.2 Estructura de un programa en Flutter
* Se ve muy por encima la estructura basica de la app ejemplo de flutter.
* Mas adelante se detallara mejor.

## 3.3 Hola Mundo en Flutter
* Vimos en la practica la estructura de un scaffold, el famoso Hola Mundo.
* Comentamos la clase __MyHomePage__ y creamos nuestro scaffold.

## 3.4 Widgtes Básicos
* [Text](https://api.flutter.dev/flutter/widgets/Text-class.html): Un objeto que nos muestra un texto. Tiene diferentes parametros que podemos ver en el enlace,
* [Row](https://api.flutter.dev/flutter/widgets/Row-class.html): acomoda en forma de filas.
* [Column](https://api.flutter.dev/flutter/widgets/Column-class.html): acomoda en forma de columnas.
* Stack: Acomoda un elemento encima de otro, lo encima.
* [Container](https://api.flutter.dev/flutter/widgets/Container-class.html): Genera un elemento como si fuera una caja. Seria como un Div de html.

## 3.5 Widgets con estado y sin estado
* __Widget con estado(StateFullWidget)__ son todos aquellos elementos de interfaz grafica con los cuales el usuario tiene accion directa en la interfaz (Checkbox, Radio, Slider, Form)
* __Widget sin estado(StateLessWidget)__ son aquellos donde el usuario no interactua con ellos. Elementos fijos que no interactuen con el usuario. (Icono, Texto, Contenedor con color)

## 3.6 Analisis de Interfaces de Usuario en Flutter
* Se enseño una metodologia para analizar las interfaces

# 4. Widgets sin estado en Flutter
## 4.1 Flutter Widgets: Container, Text, Icon, Row
* Creamos un archivo __description_place.dart__ en el cual:
  * Creamos variables que contienen los widgets y sus atributos para las secciones de star, title_star y desciption
* En el archivo __main.dart__
  * Cambiamos el contenido del body y le pasamos una nueva isntancia de la clase __DescriptionPlace__ del archivo description_place.dart

## 4.2 Flutter Widgets: Column
* En el archivo __description_place.dart__
  * Creamos 3 variables vacias(namePlace, stars, descriptionPlace).
  * Creamos el metodo constructor __DescriptionPlace__ (el constructor siempre tendra el nombre de la clase)
  * Creamos 2 variables mas (star_half, star_border) las cuales son las variaciones de las estrellas
* En el archivo __main.dart__
  * Pasamos los parametros a la instancia de la clase __DescriptionPlace__ 

## 4.3 Recursos en Flutter: Tipografias y Google Fonts
* Descargamos la fuente Lato de google fonts
* Creamos la carpeta Fonts y guardamos ahi la fuente de Lato
* En el archivo __pubspec.yaml__
  * Declaramos la fuente que vamos a usar, el nombre que va a llevar y el archivo de donde lo estamos tomando.
* En el archivo __description_place.dart__ 
  * Utilizamos la fuente Lato

## 4.4 Widget Image
* Creamos el archivo __review.dart__ 
  * Creamos la clase Revie asi como su constructor el cual recibe la imagen
  * Creamos una varibal photo que es igual a un contenedor donde pasamos la foto y sus paramteros
* Sigue en el otro capitulo

## 4.5 Widget apilando textos
* Modificamos el archivo __pubspec.yaml__ de manera que los assets apunten a todas las iamgenes
* En el archivo __main.dart__ 
  * Comentamos la clase DescriptionPlace y en su lugar ponemos RevieList esto para poder ver lo que estamos haciendo
  * Se importa el paquete review-list.dar
* En el archivo __review.dart__
  * Agregamos mas argumentos al constructor
  * Creamos diferentes varaibles que contendran imagen e info del usuario (userName, userInfo, userComment)
  * Creamos la variable userDetail que sera una columna la cual contendra las variables de info e iamgen del usuario.
  * Retornaos el userDetails
* Creamos el archivo __review-list.dart__
  * Tiene una clase ReviewList la cual retornara instancias de la clase Review(del archivo review.dart) y sus argumentos.

## 4.6 Widgets Decorados
* Creamos el archivo __gradient_back.dart__
  * Declaramos la clase GradientBack la cual nos retornara un container con un tamaño y color definido, este ultimo tendra un gradiente. 
  * Ahi se puede aprender como hacer un gradiente en flutter.
* En el archivo __main.dart__
  * Eliminamos la appBar
  * En el body llamamos a un Stack el cual contendra una ListView( la descripcion y la info), esta nos sirve para hacer el scroll y tambien al GradientBack
  * Un stack apila los widgets uno encima del otro, es por eso que el GradientBack lo delcaramos abajo ya que al hacer scroll la info se oculatara abajo del GradientBack.

## 4.7 Widget Imagen Decorada
* En el archivo GrandientBack 
  * Creamos el constructor.
  * Agregamos el Texto mediante la varible title y damos formato.
* En el archivo main.dart
  * Pasamos parametros al GradientBack
  * *SystemChrome.setEnabledSystemUIOverlays([])* sirve para que no aparezca la barra de arriba en android.
* En el archivo MainActivity.java
  * *this.getWindow().setStatusBarColor(0x00000000)* sirve para que sea transparente la barra de arriba de dispositivos android.
* Creamos el archivo card_image.dart:
  * En este archivo devolveremos la imagen en forma de card, esquinas redondeadas, sombras, etc y lo retornaremos

## 4.8 Widget Listview
* Creamos un archivo __card_image_list.dart__
  * Como su nombre lo indica, creamos una lista de CardImage
  * Creamos el widget CardImageList
  * OJO en el scrollDirection por default es vertical, pero para que el scroll fuera horitzontal utilizamos Axis.horizontal
* Creamos el archivo __header_appbar.dart__
  * Este archivo es con la finalidad de agrupar los widgets GradientBack y el CardImageList en un Stack(para que queden uno encima del otro)
  * Creamos el widget HeaderAppBar
* En el archivo __main.dart__
  * Cambiamos el widget GradientBack por HeaderAppBar

## 4.9 Widget Button, InkWell
* Creamos el archivo __button_urple.dart__
  * Creamos el widget ButtonPurple
  * Dentro del container usaramemos un InkWell el cual es como un tintero y podemos darle la forma de un boton
  * Usamos gradientes
  * Inicialmente retornamos un inkwell pero el boton tenia una zona por fuera que accioanaba el onTap, por lo cual usamos un container, se le dio margen y ya quedo
* En el archivo __description_place.dart__
  * Alienamos los widgets (crossAxisAlignment) para que queden alienados a la izquierda (start)
  * Agreagamos el widget ButtonPurple

# 5. Widgets con estado en Flutter

## 5.1 Botones en Flutter
  * [RaisedButton](https://docs.flutter.io/flutter/material/RaisedButton-class.html)
  * [FloatingActionButton](https://docs.flutter.io/flutter/material/FloatingActionButton-class.html)
  * [FlatButton](https://docs.flutter.io/flutter/material/FlatButton-class.html)
  * [IconButton](https://docs.flutter.io/flutter/material/IconButton-class.html)

## 5.2 Clase StatefulWidget: Como se compone
  * La primera clase hereda de __StateFullWidget__
  * Sobreescribimos el metodo __createState()__
  * Generamos una segunda clase privada (con el guien bajo) donde estaremos controlado el estado del widget que en este caso es dinamico
    * Esta clase hereda del __State__ la cual tendra una coleccion <>
    * Sobreescribimos el widget __build__ el cual se encarga de cosntruir y dentro de este pondremos el widget que queremos que tenga el estado
  * Un widget con estado debe tener una propiedad que permita asignarle un comportamiento cuando el estado cambie (onTap: OnTapTapped)
  * En esta funcion (onTapTapped) llamamos al __setState__ el cual es un callback el cual se ejecuta automaticamente cuando detecta que el widget declarado(BottomNavigatorBar) dentro del metodo __build__ se actualiza

## 5.3 Widget Floating Action Button
  * Creamos el archivo __floating_Action_button_green.dart__
  * Declaramos la clase __FloatingActionButtonGreen__ la cual hereda de __StatefulWidget__
    * Sobreescribimos la clase privada ___FloatingActionButtonGreenState__ para crear el estado y la retornamos
  * Creamos la clase privada ___FloatingActionButtonGreenState__ la cual hereda de State y que recibe el estado de __FloatingActionButtonGreen__
    * Creamos la funcion onPressedFav la cual se encargara de setear el estado mediante el metodo __setState__
  * Sobreescribimos el widget __build__ 
    * Retornamos el widget __FloatingActionButton__ (que es un boton de flutter por defecto), le pasamos sus parametros y le pasamos un child en el cual dependiendo el valor de _pressed cambiamos el icono.
  
## 5.4 Widgets BottomNavigationBar
  * __main.dart:__
    * Borramos el codigo y texto que estaba de mas
    * El __Stack__ que tenia el body lo pasamos al archivo __home_trips.dart__ y elimianmos el body
    * Eliminamos el __Scaffold__ del body.
  * Creamos el archivo __platzi_trips.dart__ el cual sera un archivo *StatefulWidget*
    * Tendra un Scaffold donde crearemos la barra de navegacion de la parte posteriro de la app
    * Utilizamos el widget BottomNavigationBar

## 5.5 Generando Navegacion en BottomNavigator
  * __maind.dart:__
    * Agreagamos en el home el widget __PlatziTrips__
  * __platzi_trips.dart:__
    * Cremos una variable *indexTap* la cual inicara en 0
    * Una lista de widgets (por eso ponemos el *List< Widget >* ) llamada __widgetsChildren__ los cuales seran las paginas que queremos visualizar cuando el usuario presione la opcion
    * Creamos una funcion __onTapTapped__ 
      * Que recibira un index como parametro y seteara la variable __indexTap__ 
    * En el Scaffold agregamos el body, el cual mostrara el __widgetsChildren__ con el __indexTap__ correspondiente, de inicio su valor es 0 por lo tanto el widget que se mostrara es __HomeTrips()__ 
    * En el __BottomNavigationBar__ del child
      * Agregamos el evento __onTap__ el cual llama a la funcion __onTapTapped__ que seteara el valor del indexTap con los indices de los items. No le pasamos el parametro index, ya que al hacer click sobre alguno de los botones de la barra (BottomNavigationBar) esta en automatico pasara el index.
      * Con __CurrentIndex__ , como su nombre lo indica setearemos cual es el Index actual, esto para que los iconos de la barra de navegacion vayan cambiando su color a morado segun el que presionemos.

## 5.6 Personalizando BottomNavigator Bar a Cupertino
  * Hay una lectura
  * Se crea el archivo __platzi_trips_cupertino.dart__ en el cual tenemos la estructura de nuestro Bar con Cupertino
  * Las diferencias
    * tabBuilder el cual funciona para hacer un switch y ver los casos
  * En el archivo __main.dart__ cambiamos  el home para que ahora apunte a __PlatziTripsCupertino__

## 5.7 Correr emulador ADV desde consola 
* Vamos a la carpeta __AppData\Local\Android\Sdk\emulator__
* Checamos nuestros emuladores con __emulator -list-avds__
* Corremos el emulador con __emulator @name-of-your-emulator__ 
* [Ayuda](https://www.flipandroid.com/cmo-puedo-iniciar-el-emulador-de-android-desde-la-lnea-de-comandos.html) 