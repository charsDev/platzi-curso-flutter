import 'package:flutter/material.dart';
import 'package:platzi_trips/profile_trips_button.dart';

class ProfileTripsButtons extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bookmarkButton = Container(
        child: Container(
          height: 40.0,
          width: 40.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30.0),
            color: Colors.white,
          ),
          child: Icon(
            Icons.bookmark_border,
            color: Color(0xFF4268D3),
          ),
        ));
        
    final shoppingkButton = Container(
        child: Container(
          height: 40.0,
          width: 40.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30.0),
            color: Colors.white,
          ),
          child: Icon(
            Icons.add_shopping_cart,
            color: Color(0xFF4268D3),
          ),
        ));

    final addButton = Container(
        child: Container(
          height: 60.0,
          width: 60.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30.0),
            color: Colors.white,
          ),
          child: Icon(
            Icons.add,
            color: Color(0xFF4268D3),
          ),
        ));

    final mailButton = Container(
        child: Container(
          height: 40.0,
          width: 40.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30.0),
            color: Colors.white,
          ),
          child: Icon(
            Icons.mail_outline,
            color: Color(0xFF4268D3),
          ),
        ));

    final personButton = Container(
        child: Container(
          height: 40.0,
          width: 40.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30.0),
            color: Colors.white,
          ),
          child: Icon(
            Icons.person_outline,
            color: Color(0xFF4268D3),
          ),
        ));

    final buttons = Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[bookmarkButton, shoppingkButton, addButton, mailButton, personButton],
    );

    return Container(
      margin: EdgeInsets.only(top: 190.0),
      child: buttons,
    );
  }
}
