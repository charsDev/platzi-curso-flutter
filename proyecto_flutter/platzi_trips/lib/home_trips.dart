import 'package:flutter/material.dart';
import 'package:platzi_trips/review-list.dart';

import 'description_place.dart';
import 'header_appbar.dart';

class HomeTrips extends StatelessWidget {
  String descriptionDummy ='''Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam condimentum lorem sed tempus dictum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec mattis consectetur euismod. 
  
  Nunc pellentesque mi neque, et mollis tortor placerat in. Vestibulum ornare tellus sed ante fringilla rutrum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis blandit blandit faucibus.''';
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        ListView(
          children: <Widget>[
            DescriptionPlace('Bananas', 4, descriptionDummy),
            ReviewList()
          ],
        ),
        HeaderAppBar(),
      ],
    );
  }
}
