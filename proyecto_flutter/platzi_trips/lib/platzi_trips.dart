import 'package:flutter/material.dart';
import 'package:platzi_trips/home_trips.dart';
import 'package:platzi_trips/profile_trips.dart';
import 'package:platzi_trips/search_trips.dart';

class PlatziTrips extends StatefulWidget {
  @override
  _PlatziTripsState createState() => _PlatziTripsState();
}

class _PlatziTripsState extends State<PlatziTrips> {

  int indexTap = 0;
  
  final List<Widget> widgetsChildren = [
    HomeTrips(),
    SearchTrips(),
    ProfileTrips()
  ];

  void onTapTapped(int index){
    setState(() {
      indexTap = index; 
    });
  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      body: widgetsChildren[indexTap],
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.white, // color del fondo de la barra
          primaryColor: Colors.purple // color de los iconos
        ),
        child: BottomNavigationBar(
          onTap: onTapTapped,
          currentIndex: indexTap,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('')
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search),
              title: Text('')
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text('')
            ),
          ]
        ),
      ),
    );
  }
}
