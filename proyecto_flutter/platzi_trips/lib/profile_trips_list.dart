import 'package:flutter/material.dart';
import 'package:platzi_trips/profile_trips_image.dart';

class ProfileTripsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 365.0,
      margin: EdgeInsets.only(
        top: 270.0
      ),
      // color: Colors.blue,
      child: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          ProfileTripImage('assets/images/ny.jpg', 'New York City', 'Lorem algo por algo', 'Photo and algo', 123467345),

          ProfileTripImage('assets/images/mountain-snow.jpg', 'Montañas chidas', 'Lorem algo por algo', 'Photo and algo', 123467345),

          ProfileTripImage('assets/images/lake.jpg', 'Lago Feliz', 'Lorem algo por algo', 'Photo and algo', 123467345),
        ],
      ),
    );
  }
}