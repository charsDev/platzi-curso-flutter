import 'package:flutter/material.dart';
import 'package:platzi_trips/profile_trips_floating_button.dart';

class ProfileTripImage extends StatelessWidget {

  String pathImage, title, description, tags;
  int steps;

  ProfileTripImage(this.pathImage, this.title, this.description, this.tags, this.steps);


  @override
  Widget build(BuildContext context) {

    final image = Container(
      height: 210.0,
      width: 375.0,
      margin: EdgeInsets.only(
        left: 19.0,
        right: 19.0
      ),

      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage(pathImage)
        ),
        borderRadius: BorderRadius.all(Radius.circular(20.0)),
        boxShadow: <BoxShadow>[
          BoxShadow (
            color: Colors.black38,
            blurRadius: 15.0,
            offset: Offset(0.0, 7.0)
          )
        ]
      ),
    );

    final cardText = Container(
      // color: Colors.red,
      margin: EdgeInsets.only(left: 18.0, right: 18.0, top: 8.0, bottom: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[          
          Text(
            title, 
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w800,
              fontSize: 18.0
            ),
          ),
          Spacer(),       
          Text(
            '$description\n$tags',
            style: TextStyle(
              fontSize: 11.0,
              color: Colors.grey[600]
            ),
          ),
          Spacer(),
          Text(
            'Steps $steps',
            style: TextStyle(
              color: Colors.orange[400],
              fontWeight: FontWeight.w700
            ),
          )
        ],
      ),
    );

    final card = Container(
      height: 100.0,
      width: 260.0,
      margin: EdgeInsets.only(left: 75.0, right: 75.0, top: 158.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.white,
        boxShadow: <BoxShadow> [
          BoxShadow (offset: Offset(0.0, 7.0), color: Colors.black38, blurRadius: 15.0)
        ]
      ),
      child: cardText,
    );

    return Stack(
      children: <Widget>[
        image,
        card,
        ProfileTripsFloatingButton()
      ],
    );
  }
}