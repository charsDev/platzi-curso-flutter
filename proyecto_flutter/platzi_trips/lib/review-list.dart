import 'package:flutter/material.dart';
import 'package:platzi_trips/review.dart';

class ReviewList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Review("assets/images/fondo.jpg", "Vanura Yasas", "1 review 5 photos", "There is an amazing place in Sri lanka"),
        Review("assets/images/scarlett.jpg", "Juana Perez", "2 review 53 photos", "Nada que decir por ahora"),
        Review("assets/images/saitama.jpg", "Carlos Garcia", "10 review 15 photos", "Aprendiendo Flutter con Platzi")
      ],
    );
  }
}