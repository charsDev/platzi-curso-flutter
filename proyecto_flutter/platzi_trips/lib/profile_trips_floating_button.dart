import 'package:flutter/material.dart';

class ProfileTripsFloatingButton extends StatefulWidget {
  @override
  _ProfileTripsFloatingButtonState createState() => _ProfileTripsFloatingButtonState();
}

class _ProfileTripsFloatingButtonState extends State<ProfileTripsFloatingButton> {

  bool _pressed = false;

  void onPressedFav(){
    setState(() {
     _pressed = !this._pressed; 
    });
  }

  @override
  Widget build(BuildContext context) {

    final heart = FloatingActionButton(
      backgroundColor: Colors.green,
      mini: true,
      tooltip: 'Fav',
      onPressed: onPressedFav,
      child: Icon(
        this._pressed ? Icons.favorite : Icons.favorite_border
      ),
    );

    return Container(
      margin: EdgeInsets.only(
        top: 230.0,
        left: 270.0
      ),
      child: heart,
    );
  }

}