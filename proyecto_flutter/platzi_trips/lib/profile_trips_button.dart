import 'package:flutter/material.dart';

class ProfileTripButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 230.0),
      child: Container(
        height: 40.0,
        width: 40.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          color: Colors.white,
        ),
        child: Icon(
          Icons.bookmark_border,
          color: Color(0xFF4268D3),
        ),
      ),
    );
  }
}
