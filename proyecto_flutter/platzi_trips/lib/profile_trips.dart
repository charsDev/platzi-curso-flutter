import 'package:flutter/material.dart';
import 'package:platzi_trips/gradient_back.dart';
import 'package:platzi_trips/profile_trips_buttons.dart';
import 'package:platzi_trips/profile_trips_list.dart';
import 'package:platzi_trips/profile_trips_user.dart';

class ProfileTrips extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        GradientBack('Profile', 400.0, -0.75),
        ProfileTripsUser(),
        ProfileTripsButtons(),
        ProfileTripsList()
      ],
    );
  }
}