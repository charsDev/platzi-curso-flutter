import 'package:flutter/material.dart';

class ProfileTripsUser extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final profilePhoto = Container(
      margin: EdgeInsets.only(top: 1.5, left: 15.0),
      width: 70,
      height: 70,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage('assets/images/saitama.jpg'))),
    );

    final userName = Container(
      margin: EdgeInsets.only(left: 10.0, top: 15.0),
      child: Text(
        'Pathum Tzoo',
        style: TextStyle(
          color: Colors.white,
          fontSize: 18.0,
          fontFamily: 'Lato',
          fontWeight: FontWeight.w400
        ),
      ),
    );

    final userEmail = Container(
      margin: EdgeInsets.only(left: 10.0, top: 5.0),
      child: Text(
        'algo@hotmail.com',
        style: TextStyle(
          color: Colors.grey[500]
        ),
      ),
    );

    final profileUserDetails = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[userName, userEmail],
    );

    return Container(
      height: 80.0,
      margin: EdgeInsets.only(top: 95.0),
      child: Row(
        children: <Widget>[profilePhoto, profileUserDetails],
      ),
    );
  }
}
