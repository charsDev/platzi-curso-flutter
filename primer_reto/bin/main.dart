void main() {
  var ages = [33, 15, 27, 40, 22];
  ages.sort();

  var lastAge = ages.last;
  var firstAge = ages.first;

  var sum = 0;
  ages.forEach((age) => sum += age);
  var prom = sum/ages.length;

  print('El primer numero es: ${firstAge}');
  print('El ultimo numero es: ${lastAge}');
  print('El promedio es: ${prom}');

}
